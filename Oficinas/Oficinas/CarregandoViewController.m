//
//  CarregandoViewController.m
//  Oficinas
//
//  Created by Pedro Brandão on 05/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import "CarregandoViewController.h"
#import "OficinaController.h"

@interface CarregandoViewController () {
    
    OficinaController *controller;
}

@end

@implementation CarregandoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.progressView setProgress:0];
    [self.activityIndicator startAnimating];
    
    controller = [OficinaController sharedController];
    [controller loadOficinasInView:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
