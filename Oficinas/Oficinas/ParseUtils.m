//
//  ParseUtils.m
//  Oficinas
//
//  Created by Pedro Brandão on 05/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import "ParseUtils.h"

@implementation ParseUtils

/**
 * @brief Retorna uma imagem de uma string base64Encoded
 */
+(UIImage *)imageFromBase64string:(NSString *)string {
    
    NSData *imageData = [[NSData alloc] initWithBase64EncodedString:string options:0];
    return [UIImage imageWithData:imageData];
}

/**
 * @brief Retorna string base64Encoded de uma imagem
 */
+(NSString *)stringFromUImage:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
}

/**
 * @brief O parse do AFNetworking retorna um dicionario que pode conter strings "<null>". Ao obter o objeto e posicioná-lo em um NSString, é gerado um erro em tempo de execução. Foi utilizado o método stringWithFormat para contornar o problema, até uma solução mais apropriada.
 */
+(NSString *)filtraInput:(NSString *)input {
    
    NSString *filtrado = [NSString stringWithFormat:@"%@", input];
    
    if ([filtrado isEqualToString:@"<null>"] || [filtrado isEqualToString:@"(null)"]) {
        filtrado = @"";
    }
    
    return filtrado;
}

@end
