//
//  Indicacao.h
//  Oficinas
//
//  Created by Pedro Brandão on 04/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface Indicacao : JSONModel

@property (assign, nonatomic) NSInteger codigoAssociacao;
@property (retain, nonatomic) NSString *dataCriacao;
@property (retain, nonatomic) NSString *cpfAssociado;
@property (retain, nonatomic) NSString *emailAssociado;
@property (retain, nonatomic) NSString *nomeAssociado;
@property (retain, nonatomic) NSString *telefoneAssociado;
@property (retain, nonatomic) NSString *placaVeiculoAssociado;
@property (retain, nonatomic) NSString *nomeAmigo;
@property (retain, nonatomic) NSString *telefoneAmigo;
@property (retain, nonatomic) NSString *emailAmigo;
@property (retain, nonatomic) NSString *observacao;

@end
