//
//  OficinaController.h
//  Oficinas
//
//  Created by Pedro Brandão on 04/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Oficina.h"
#import "CarregandoViewController.h"

@interface OficinaController : NSObject

@property (nonatomic,retain) NSArray<Oficina *> *oficinas;

+(OficinaController *)sharedController;

-(void)loadOficinasInView:(CarregandoViewController *)carregandoView;

@end
