//
//  Oficina.m
//  Oficinas
//
//  Created by Pedro Brandão on 04/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import "Oficina.h"
#import "ParseUtils.h"

@implementation Oficina

-(instancetype)initComDicionario:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        _Id = [[dic objectForKey:@"Id"] integerValue];
        _nome = [ParseUtils filtraInput:[dic objectForKey:@"Nome"]];
        _descricao = [ParseUtils filtraInput:[dic objectForKey:@"Descricao"]];
        _descricaoCurta = [ParseUtils filtraInput:[dic objectForKey:@"DescricaoCurta"]];
        _endereco = [ParseUtils filtraInput:[dic objectForKey:@"Endereco"]];
        _latitude = [[dic objectForKey:@"Latitude"] floatValue];
        _longitude = [[dic objectForKey:@"Longitude"] floatValue];
        _foto = [ParseUtils imageFromBase64string:[dic objectForKey:@"Foto"]];
        _avaliacaoUsuario = [[dic objectForKey:@"Avaliacao"] integerValue];
        _email = [ParseUtils filtraInput:[dic objectForKey:@"Email"]];
        _fone1 = [ParseUtils filtraInput:[dic objectForKey:@"Telefone1"]];
        _fone2 = [ParseUtils filtraInput:[dic objectForKey:@"Telefone2"]];
        _ativo = [[dic objectForKey:@"Ativo"] boolValue];
    }
    return self;
}

@end
