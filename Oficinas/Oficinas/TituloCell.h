//
//  TituloCell.h
//  Oficinas
//
//  Created by Pedro Brandão on 05/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TituloCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *foto;
@property (weak, nonatomic) IBOutlet UILabel *nome;
@property (weak, nonatomic) IBOutlet UILabel *descricao;

@end
