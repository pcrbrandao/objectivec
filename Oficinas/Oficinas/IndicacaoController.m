//
//  IndicacaoController.m
//  Oficinas
//
//  Created by Pedro Brandão on 05/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import "IndicacaoController.h"
#import "Indicacao.h"
#import "EntradaIndicacao.h"
#import <AFNetworking.h>
#import "Retorno.h"
#import "RetornoErro.h"
#import "ParseUtils.h"

@implementation IndicacaoController

/**
 * @brief É necessário para que o singleton funcione
 */
static IndicacaoController *sharedController = nil;

/**
 * @brief Retorna uma única instância
 */
+(IndicacaoController *)sharedController {
    if (!sharedController) {
        sharedController = [[IndicacaoController alloc] init];
    }
    
    return sharedController;
}

/**
 * @brief O post vai sair daqui
 */
-(void)postIndicacaoInView:(IndicarViewController *)indicarVC {
    
    NSString *url = @"http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/Api/Indicacao";
    
    NSURL *URL = [NSURL URLWithString:url];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    EntradaIndicacao *entrada = [self indicacaoNaView:indicarVC];
    NSDictionary *params = entrada.toDictionary;
    
    [manager POST:URL.absoluteString parameters:params
         progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             
             // tudo certo! volta para tela anterior
             UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                 [indicarVC.navigationController popViewControllerAnimated:YES];
             }];
             
             Retorno *retorno = [self retornoDoDic:responseObject];
             NSLog(@"\n\na resposta....\n%@", retorno);
             
             if ([retorno.erro.retornoErro length] == 0) {
                 
                 [self showMessage:retorno.sucesso withAction:ok InView:indicarVC];
                 
             } else {
                 
                 UIAlertAction *falhaAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                     NSLog(@"Error: %@", retorno.erro.retornoErro);
                 }];
                 
                 [self showMessage:retorno.erro.retornoErro withAction:falhaAction InView:indicarVC];
             }
         }
         failure:^(NSURLSessionTask *operation, NSError *error) {
             
             UIAlertAction *falha = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                NSLog(@"Error: %@", error);
             }];
             
             [self showMessage:@"Um erro aqui" withAction:falha InView:indicarVC];
         }];
}

-(Retorno *)retornoDoDic:(NSDictionary *)dic {
    NSDictionary *retornoDic = [dic objectForKey:@"RetornoErro"];
    NSString *sucesso = [ParseUtils filtraInput:[retornoDic objectForKey:@"Sucesso"]];
    
    Retorno *retorno = [[Retorno alloc] init];
    retorno.erro = [self retornoErroDoDic:dic];
    retorno.sucesso = sucesso;
    
    return retorno;
}

-(RetornoErro *)retornoErroDoDic:(NSDictionary *)dic {
    NSDictionary *retornoErroDic = [dic objectForKey:@"RetornoErro"];
    NSString *erro = [ParseUtils filtraInput:[retornoErroDic objectForKey:@"retornoErro"]];
    
    if ([erro length] == 0) {
        return nil;
    }
    
    RetornoErro *retornoErro = [[RetornoErro alloc] init];
    retornoErro.retornoErro = erro;
    
    return retornoErro;
}

/**
 * @brief Envia um alerta de ok e, depois de pressionar ok, volta para tela anterior
 */
-(void)showMessage:(NSString *)mensagem withAction:(UIAlertAction *)action InView:(UIViewController *)viewController {
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Enviado" message:mensagem preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:action];
    [viewController presentViewController:alert animated:YES completion:nil];
}

/**
 * @brief Obter os dados na view e retornar um objeto Indicacao. Para simplificar, não será feito verificação dos dados ainda, que presumimos serem válidos.
 */
-(EntradaIndicacao *)indicacaoNaView:(IndicarViewController *)indicarVC {
    
    Indicacao *indicacao = [[Indicacao alloc] init];
    indicacao.codigoAssociacao = [indicarVC.codigoAssociado.text integerValue];
    indicacao.dataCriacao = [indicarVC.codigoAssociado text];
    indicacao.cpfAssociado = [indicarVC.cpfAssociado text];
    indicacao.emailAssociado = [indicarVC.emailAssociado text];
    indicacao.nomeAssociado = [indicarVC.nomeAssociado text];
    indicacao.telefoneAssociado = [indicarVC.telefoneAssociado text];
    indicacao.placaVeiculoAssociado = [indicarVC.placaVeiculoAssociado text];
    indicacao.nomeAmigo = [indicarVC.nomeAmigo text];
    indicacao.telefoneAmigo = [indicarVC.telefoneAmigo text];
    indicacao.emailAmigo = [indicarVC.emailAmigo text];
    indicacao.observacao = [indicarVC.observacao text];
    
    EntradaIndicacao *entrada = [[EntradaIndicacao alloc] init];
    entrada.remetente = [indicarVC.remetente text];
    
    entrada.indicacao = indicacao;
    entrada.copias = @[];
    
    return entrada;
}

@end
