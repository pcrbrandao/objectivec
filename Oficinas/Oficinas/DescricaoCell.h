//
//  DescricaoCell.h
//  Oficinas
//
//  Created by Pedro Brandão on 05/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescricaoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *descricao;

@end
