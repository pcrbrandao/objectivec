//
//  DetailViewController.h
//  Oficinas
//
//  Created by Pedro Brandão on 04/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Oficina.h"

@interface DetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) Oficina *oficina;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

