//
//  EntradaIndicacao.h
//  Oficinas
//
//  Created by Pedro Brandão on 04/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "Indicacao.h"

@interface EntradaIndicacao : JSONModel

@property (retain, nonatomic) Indicacao *indicacao;
@property (retain, nonatomic) NSString *remetente;
@property (retain, nonatomic) NSArray<NSString *> *copias;

@end
