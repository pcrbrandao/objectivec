//
//  ParseUtils.h
//  Oficinas
//
//  Created by Pedro Brandão on 05/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ParseUtils : NSObject

+(UIImage *)imageFromBase64string:(NSString *)string;
+(NSString *)stringFromUImage:(UIImage *)image;
+(NSString *)filtraInput:(NSString *)input;

@end
