//
//  IndicarViewController.h
//  Oficinas
//
//  Created by Pedro Brandão on 05/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndicarViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *codigoAssociado;
@property (weak, nonatomic) IBOutlet UITextField *dataCriacao;
@property (weak, nonatomic) IBOutlet UITextField *cpfAssociado;
@property (weak, nonatomic) IBOutlet UITextField *emailAssociado;
@property (weak, nonatomic) IBOutlet UITextField *nomeAssociado;
@property (weak, nonatomic) IBOutlet UITextField *telefoneAssociado;
@property (weak, nonatomic) IBOutlet UITextField *placaVeiculoAssociado;
@property (weak, nonatomic) IBOutlet UITextField *nomeAmigo;
@property (weak, nonatomic) IBOutlet UITextField *telefoneAmigo;
@property (weak, nonatomic) IBOutlet UITextField *emailAmigo;
@property (weak, nonatomic) IBOutlet UITextField *remetente;
@property (weak, nonatomic) IBOutlet UITextField *observacao;

- (IBAction)enviar:(id)sender;

@end
