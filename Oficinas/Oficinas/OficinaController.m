//
//  OficinaController.m
//  Oficinas
//
//  Created by Pedro Brandão on 04/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import "OficinaController.h"
#import "AFNetworking.h"
#import "Oficina.h"


@implementation OficinaController

/**
 * @brief Faz a requisição, atualiza as UIs e a lista de oficinas
 */
-(void)loadOficinasInView:(CarregandoViewController *)carregandoView {
    if (!_oficinas) {
        
        NSString *url = @"http://app.hinovamobile.com.br/ProvaConhecimentoWebApi/Api/Oficina?codigoAssociacao=601&cpfAssociado=''";
        
        NSURL *URL = [NSURL URLWithString:url];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager GET:URL.absoluteString parameters:nil
            progress:^(NSProgress *progress) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [carregandoView.progressView setProgress:[progress fractionCompleted]];
                });
            }
             success:^(NSURLSessionTask *task, id responseObject) {
                 [carregandoView.activityIndicator stopAnimating];
                 [carregandoView.mensagem setText:@"Tudo certo!"];
                 
                 _oficinas = [self oficinasWithDictionary:(NSDictionary *)responseObject];
                 [carregandoView performSegueWithIdentifier:@"segueMain" sender:carregandoView];
             }
             failure:^(NSURLSessionTask *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
                 _oficinas = nil;
        }];
    }
}

/**
 * @brief Retorna uma lista de oficinas a partir de um dicionario
 */
-(NSArray<Oficina *> *)oficinasWithDictionary:(NSDictionary *)dic {
    
    NSArray *lista = [dic objectForKey:@"ListaOficinas"];
    NSMutableArray<Oficina *> *listaOficina = [[NSMutableArray alloc] init];
    
    for (NSDictionary *oficinaDic in lista) {
        Oficina *oficina = [[Oficina alloc] initComDicionario:oficinaDic];
        NSLog(@"Oficina acrescentada à lista..... %@", oficina.descricaoCurta);
        [listaOficina addObject:oficina];
    }
    return listaOficina;
}

/**
 * @brief É necessário para que o singleton funcione
 */
static OficinaController *sharedController = nil;

/**
 * @brief Retorna uma única instância
 */
+(OficinaController *)sharedController {
    if (!sharedController) {
        sharedController = [[OficinaController alloc] init];
    }
    
    return sharedController;
}

@end
