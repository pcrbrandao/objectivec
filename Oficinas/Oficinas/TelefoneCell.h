//
//  TelefoneCell.h
//  Oficinas
//
//  Created by Pedro Brandão on 05/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TelefoneCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *telefone1;
@property (weak, nonatomic) IBOutlet UILabel *telefone2;

@end
