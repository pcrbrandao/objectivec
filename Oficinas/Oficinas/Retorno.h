//
//  Retorno.h
//  Oficinas
//
//  Created by Pedro Brandão on 04/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "RetornoErro.h"

@interface Retorno : JSONModel

@property (retain, nonatomic) RetornoErro *erro;
@property (retain, nonatomic) NSString *sucesso;

@end
