//
//  RetornoErro.h
//  Oficinas
//
//  Created by Pedro Brandão on 04/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface RetornoErro : JSONModel

@property (retain, nonatomic) NSString *retornoErro;

@end
