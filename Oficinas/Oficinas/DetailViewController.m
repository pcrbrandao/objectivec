//
//  DetailViewController.m
//  Oficinas
//
//  Created by Pedro Brandão on 04/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import "DetailViewController.h"
#import "TituloCell.h"
#import "DescricaoCell.h"
#import "EnderecoCell.h"
#import "TelefoneCell.h"
#import "AvaliacaoCell.h"

@interface DetailViewController () {
    NSArray *dados;
}

@end

@implementation DetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    dados = @[@"titulo", @"descricao", @"endereco", @"telefone", @"avaliacao", @"indicar"];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self.tableView setEstimatedRowHeight:100];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dados count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger row = indexPath.row;
    
    if (row == 0) {
        TituloCell *tituloCell = [tableView dequeueReusableCellWithIdentifier:dados[row]];
        [tituloCell.foto setImage:self.oficina.foto];
        [tituloCell.nome setText:self.oficina.nome];
        [tituloCell.descricao setText:self.oficina.descricaoCurta];
        
        return tituloCell;
    }
    
    if (row == 1) {
        DescricaoCell *descricaoCell = [tableView dequeueReusableCellWithIdentifier:dados[row]];
        [descricaoCell.descricao setText:self.oficina.descricao];
        
        return descricaoCell;
    }
    
    if (row == 2) {
        EnderecoCell *enderecoCell = [tableView dequeueReusableCellWithIdentifier:dados[row]];
        [enderecoCell.endereco setText:self.oficina.endereco];
        
        return enderecoCell;
    }
    
    if (row == 3) {
        TelefoneCell *telefoneCell = [tableView dequeueReusableCellWithIdentifier:dados[row]];

        [telefoneCell.telefone1 setText:[self.oficina fone1]];
        [telefoneCell.telefone2 setText:[self.oficina fone2]];

        return telefoneCell;
    }
    
    if (row == 4) {
        AvaliacaoCell *avaliacaoCell = [tableView dequeueReusableCellWithIdentifier:dados[row]];
        [avaliacaoCell.avaliacao setText:[NSString stringWithFormat:@"%ld", (long)self.oficina.avaliacaoUsuario]];
        
        return avaliacaoCell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dados[indexPath.row]];
    return cell;
}


@end
