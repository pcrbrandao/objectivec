//
//  IndicacaoController.h
//  Oficinas
//
//  Created by Pedro Brandão on 05/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IndicarViewController.h"

@interface IndicacaoController : NSObject

-(void)postIndicacaoInView:(IndicarViewController *)indicarVC;

+(IndicacaoController *)sharedController;

@end
