//
//  Oficina.h
//  Oficinas
//
//  Created by Pedro Brandão on 04/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSONModel.h>

@interface Oficina : JSONModel

@property (assign, nonatomic) NSInteger Id;
@property (retain, nonatomic) NSString *nome;
@property (retain, nonatomic) NSString *descricao;
@property (retain, nonatomic) NSString *descricaoCurta;
@property (retain, nonatomic) NSString *endereco;
@property (assign, nonatomic) float latitude;
@property (assign, nonatomic) float longitude;
@property (retain, nonatomic) UIImage *foto;
@property (assign, nonatomic) NSInteger avaliacaoUsuario;
@property (retain, nonatomic) NSString *email;
@property (retain, nonatomic) NSString *fone1;
@property (retain, nonatomic) NSString *fone2;
@property (assign, nonatomic) BOOL ativo;

-(instancetype)initComDicionario:(NSDictionary *)dic;

@end
