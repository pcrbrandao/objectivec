//
//  EnderecoCell.h
//  Oficinas
//
//  Created by Pedro Brandão on 05/03/17.
//  Copyright © 2017 Pedro Brandão. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnderecoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *endereco;

@end
